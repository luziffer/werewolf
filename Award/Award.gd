extends TextureRect

export(NodePath) var _Template 
onready var Template = get_node(_Template)

export(Texture) var bg_village
export(Texture) var bg_wolves
export(Texture) var icon_alive
export(Texture) var icon_dead

var _winners = []

func lower_score(id1, id2):
	var f1 = Network.get_data(id1, "factions")
	var f2 = Network.get_data(id2, "factions")
	var w1 = false
	var w2 = false
	for f in f1:
		if f in _winners: w1 = true
	for f in f2:
		if f in _winners: w2 = true
	if w1 == w2:
	
		var d1 = Network.get_data(id1, "killed")
		var d2 = Network.get_data(id2, "killed")
		if d1 == d2:
			
			var n1 = Network.get_data(id1, "name")
			var n2 = Network.get_data(id2, "name")
			return n2 < n1
			
		elif id1 == -1: return false
		elif id2 == -1: return true
		else: 
			return d1 <= d2
	elif w1: return true
	elif w2: return false
	 
	
func _ready():
	var factions = Life.get_alive_factions()
	if len(factions) == 1:
		var faction = factions[0]
		if faction == "Village":
			texture = bg_village
		elif faction == "Werewolves":
			texture = bg_wolves
	_winners = factions
	
	Template.hide()
	var list = Template.get_parent()
	var ids = Network.get_ids()
	ids.sort_custom(self, "lower_score")
	for id in ids:
		var node = Template.duplicate(true)
		node.name = String(id)
		
		# Name and portrait
		node.get_node("Name").text = Network.get_data(id, "name")
		node.get_node("Portrait").texture = load(Network.get_data(id, "portrait"))
		
		# Role
		var role = Network.get_data(id, "role")
		node.get_node("Role").texture = Roles.Definitions[role]["tex"]
		node.get_node("Role/Label").text = role
		
		# Role
		var img = icon_alive if Life.is_alive(id) else icon_dead
		node.get_node("Life").texture = img
		
		
		node.show()
		list.add_child(node)


func _on_back():
	Network.restart()
