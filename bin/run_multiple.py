#!/usr/bin/env python

import os
import time
import subprocess as sp
from math import ceil,sqrt

from argparse import ArgumentParser
from screeninfo import get_monitors

# Name of binary
binary = "Werewolf.x86_64"

#plattform = os. ... ?
#if plattform != linux:
#...
#

local = os.path.abspath(__file__)
local = os.path.dirname(local)

def get_args():
    args = ArgumentParser()
    args.add_argument("-n","--number", default=0, type=int, help="Number of players.")
    args.add_argument("--build", action="store_true", help="Build export.")
    args.add_argument("--close", action="store_true", help="Close all existing windows.")
    return args.parse_args()


# Processess
def open_windows(number):
    cmd = os.path.join(local, binary)
    for i in range(number):
        sp.Popen([cmd])
    time.sleep(.5 * number)

def close_windows():
    sp.call(["killall", binary])


# Window managment 
def get_ids_by_name(name=""):
    if name == "":
        name = os.path.splitext(binary)[0]
    ids = []
    apps = sp.check_output(["wmctrl", "-lp"]).decode("utf-8").split("\n")
    for app in apps:
        args = app.split(" ")
        if len(args) >= 2:
            if args[-1] == name:
                ids.append(args[0])
    return ids

def reshape_window(id, pos, size):    
    
    # Calculate relative positions
    screens = get_monitors()
    w_total = max([s.width + s.x  for s in screens])
    h_total = max([s.height + s.y for s in screens])
    total = (w_total, h_total)
    pos  = [ str(int(100*pos[i]/total[i]))  + "%" for i in range(2)]
    size = [ str(int(100*size[i]/total[i])) + "%" for i in range(2)]

    sp.call(["xdotool", "windowsize", id, *size]) 
    sp.call(["xdotool", "windowmove", id, *pos])

def adjust_windows():
    ids = get_ids_by_name()
    if len(ids) == 0: return
    
    # Disrtibute on grid across monitor screens 
    screens = get_monitors()
    ids_per_screen = int(ceil(len(ids) / len(screens)))
    
    columns = int(sqrt(ids_per_screen))
    rows = int(ceil(ids_per_screen/columns))
    print("Found %d windows. Arranging in %d x %d x %d grid." % (len(ids), ids_per_screen, columns, rows)) 
    for (n, id) in enumerate(ids):
       
       # Screen Index 
       s = n // ids_per_screen
       screen = screens[s]
       w,h = screen.width//rows, screen.height//columns
       x,y = screen.x, screen.y
       
       # Grid Indices
       k = n % ids_per_screen
       i = k % rows
       j = k // rows
       u = x + w * i
       v = y + h * j
       
       reshape_window(id, (u,v), (w, h))

# Run compiler implicitly 
def build_game():
    curdir = os.getcwd()
    project = os.path.dirname(local)
    os.chdir(project)
    sp.call(["godot", "project.godot", "--export", "Linux/X11", os.path.join(".", "bin", binary)])
    os.chdir(curdir)

if __name__ == "__main__":
    args = get_args()
    if args.close:
        close_windows()
    if args.build:
        build_game()
    if args.number > 0:
        open_windows(args.number)
    adjust_windows()
      
  
  
