extends Panel


func _ready():
	Network.connect("data_changed", self, "_update_data")
	
	var id = get_tree().get_network_unique_id()
	for key in Network._data[id]:
		_update_data(id, key)

func _update_data(id, key):
	if id == get_tree().get_network_unique_id():
		
		if key == "name":
			var name = Network.get_own_data("name")
			get_node("HBoxContainer/Identity/Name").text = name
		
		elif key == "portrait":
			var portrait = Network.get_own_data("portrait")
			portrait = load(portrait)
			get_node("HBoxContainer/Identity/Portrait").texture = portrait
			
		elif key == "role":
			var role = Network.get_own_data("role")
			get_node("HBoxContainer/Character/Role").texture = Roles.Definitions[role]["tex"]
			get_node("HBoxContainer/Character/Role/Label").text = role
			get_node("HBoxContainer/Character/RoleState").hide()
		
		elif key == "role_state":
			var role = Network.get_own_data("role")
			var role_state = Network.get_own_data("role_state")
			
			var label = get_node("HBoxContainer/Character/RoleState")
			if role == "Witch":
				var msg = ""
				msg += "[color=green]\tHealing: [/color]\t" + String(role_state["heal"]) + "\n"
				msg += "[color=red]\tPoison:  [/color]\t" + String(role_state["poison"]) + "\n"
				label.bbcode_text = msg
				label.show()
				
			else:
				label.hide()
		
		elif key == "killed":
			var alive = Network.get_own_data("killed") == -1
			if alive:
				get_node("HBoxContainer/Identity/Portrait/Dead").hide()
			else:
				get_node("HBoxContainer/Identity/Portrait/Dead").show()
