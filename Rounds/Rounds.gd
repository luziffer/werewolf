extends Control

func _ready():
	set_process(get_tree().is_network_server())

func check_if_finished():
	var factions = Life.get_alive_factions()
	var finished = false 
	if len(factions) == 1:
		finished = true
	return finished

func kill_victim(id):
	if id != -2:
		var game_state = Network.get_data(1, "game_state")
		Network.set_data(id, "killed", game_state["round"])
		
		# Roles of dead go public 
		for other in Network.get_ids():
			var memory = Network.get_data(other, "memory").duplicate(true)
			if not id in memory:
				memory.append(id)
				Network.set_data(other, "memory", memory)

# Game loop only runs on host
func _process(_delta):
	if check_if_finished():
		Network.end_game()
		return
	
	var game_state = Network.get_data(1, "game_state")
	if game_state["is_night"]:
		var finished = true
		
		# Handle hunt
		var werewolves = Life.get_alive_with({"role" : "Werewolf"})
		if game_state["victim"] == -1:
			finished = false
			if not Decisions.have_finished(werewolves):
				for id in werewolves:
					if Network.get_data(id, "decision")["action"] == "":
						Network.set_data(id, "decision", {
							"answer" : [],
							"type"   : "vote",
							"action" : "start",
							"params" : ["exclusive"]
						})
			else:
				var responses = Decisions.get_decisions(werewolves)
				var victim = Decisions.get_winner(responses, true)
				victim = victim[0] if len(victim) == 1 else -2
				game_state["victim"] = victim
				Network.set_data(1, "game_state", game_state)
		
		# Handle observing
		var seers = Life.get_alive_with({"role" : "Seer"})
		for seer in seers:
			var r = Network.get_data(seer, "role_state")
			if r["observee"] == -1:
				finished = false
				if not Decisions.have_finished([seer]):
					if Network.get_data(seer, "decision")["action"] == "":
						Network.set_data(seer, "decision", {
							"answer" : [],
							"type"   : "vote",
							"action" : "start",
							"params" : ["exclusive"]
						})
				else:
					var responses = Decisions.get_decisions([seer])
					var observee = Decisions.get_winner(responses, false)
					r["observee"] = observee
					Network.set_data(seer, "role_state", r)
		
		# Handle potions
		var witches = Life.get_alive_with({"role" : "Witch"})
		for witch in witches:
			var r = Network.get_data(witch, "role_state")
			
			if r["rescuee"] == -1:
				finished = false 
				if game_state["victim"] != -1:
					if r["heal"] == 0: 
						r["rescuee"] = -2
						Network.set_data(witch, "role_state", r)
					else:
						if not Decisions.have_finished([witch]):
							if Network.get_data(witch, "decision")["action"] == "":
								Network.set_data(witch, "decision", {
									"answer" : -1,
									"type"   : "question",
									"action" : "start",
									"params" : [] 
								})
						else:
							var responses = Decisions.get_decisions([witch])
							var rescuee = Decisions.get_winner(responses, false)
							r["rescuee"] = rescuee
							if rescuee != -2:
								r["heal"] -= 1
							Network.set_data(witch, "role_state", r)
			elif r["victim"] == -1:
				finished = false
				if r["poison"] == 0:
					r["victim"] = -2
					Network.set_data(witch, "role_state", r)
				else:
					if not Decisions.have_finished([witch]):
						if Network.get_data(witch, "decision")["action"] == "":
							Network.set_data(witch, "decision", {
								"answer" : [],
								"type"   : "vote",
								"action" : "start",
								"params" : ["exclusive"]
							})
					else:
						var responses = Decisions.get_decisions([witch])
						var victim = Decisions.get_winner(responses, false)
						r["victim"] = victim
						if victim != -2:
							r["poison"] -= 1
						Network.set_data(witch, "role_state", r)
		
		# change to day
		if finished:
			
			# Witches rescue
			for witch in witches:
				var r = Network.get_data(witch, "role_state")
				if game_state["victim"] == r["rescuee"]:
					game_state["victim"] = -2
			
			kill_victim(game_state["victim"])
			
			# Witches poison 
			for witch in witches:
				var r = Network.get_data(witch, "role_state")
				if game_state["victim"] != r["victim"]:
					kill_victim(r["victim"])
			
			# Update seers memory
			for seer in seers:
				if Life.is_alive(seer):
					var memory = Network.get_data(seer, "memory").duplicate(true)
					var r = Network.get_data(seer, "role_state")
					if r["observee"] != -2 and not r["observee"] in memory:
						memory.append(r["observee"])  
						Network.set_data(seer, "memory", memory)
					
			# Switch game state
			game_state["round"] += 1
			game_state["is_night"] = false
			game_state["victim"] = -1
			Network.set_data(1, "game_state", game_state)
	
	else:
		var finished = true
		
		# public execution
		var alive = Life.get_alive()
		if game_state["victim"] == -1:
			finished = false
			if not Decisions.have_finished(alive):
				for id in alive:
					if Network.get_data(id, "decision")["action"] == "":
						Network.set_data(id, "decision", {
							"answer" : [],
							"type" : "vote",
							"action" : "start",
							"timed" : false,
							"params" : []
						})
			else:
				var responses = Decisions.get_decisions(alive)
				var victim = Decisions.get_winner(responses, true)
				victim = victim[0] if len(victim) == 1 else -2
				game_state["victim"] = victim
				Network.set_data(1, "game_state", game_state)
		
		# change to night
		if finished:
			kill_victim(game_state["victim"])
			
			# Reset seers
			for seer in Life.get_alive_with({"role" : "Seer"}):
				var r = Network.get_data(seer, "role_state")
				r["observee"] = -1
				Network.set_data(seer, "role_state", r)
			
			# Reset witches
			for witch in Life.get_alive_with({"role" : "Witch"}):
				var r = Network.get_data(witch, "role_state")
				r["rescuee"] = -1
				r["victim"] = -1
				Network.set_data(witch, "role_state", r)
			
			
			# Switch game state
			game_state["round"] += 1
			game_state["is_night"] = true
			game_state["victim"] = -1
			Network.set_data(1, "game_state", game_state)
