extends Label

onready var _oldtext = String(self.text)

func _ready():
	Network.connect("data_changed", self, "_update_data")
	_update_data(1, "game_state")

func _update_data(id, key):
	if id == 1 and key == "game_state":
		var game_state = Network.get_data(1, "game_state")
		text = String(_oldtext)
		text = text.replace("ROUND", String(game_state["round"]))
		text = text.replace("DAYTIME", "Night time" if game_state["is_night"] else "Day time")
		
