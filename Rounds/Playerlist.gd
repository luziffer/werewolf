extends Panel


func _ready():
	var list = $ScrollContainer/List
	var template = list.get_node("Template")
	template.hide()
	
	for id in Network.get_ids():
		var node = template.duplicate(true)
		node.name = String(id)
		
		# Set name and portrait
		node.get_node("Name").text = Network.get_data(id, "name")
		var portrait = Network.get_data(id, "portrait")
		node.get_node("Portrait").texture = load(portrait)
		
		# Set Role 
		var role = Network.get_data(id, "role")
		node.get_node("Role/Label").text = role
		node.get_node("Role").texture = Roles.Definitions[role]["tex"]
		node.get_node("Role").hide()
		
		# Selector
		var select = node.get_node("Select")
		select.hide()
		select.connect("toggled", self, "_on_toggle_select", [id])
		
		list.add_child(node)
		node.show()
	
	# update data routines
	Network.connect("data_changed", self, "_update_data")
	for id in Network.get_ids():
		for k in Network._data[id]:
			_update_data(id, k)

func _update_data(id, key):
	var list = $ScrollContainer/List
	var node = list.get_node(String(id))
	
	
	if key == "killed":
		var dead = node.get_node("Portrait/Dead")
		var select = node.get_node("Select")
		if Network.get_data(id, "killed") == -1:
			dead.hide()
			select.disabled = false
		else:
			dead.show()
			select.disabled = true 
	
	if id == get_tree().get_network_unique_id():
		
		# known identities
		if key == "memory":
			for other in Network.get_own_data("memory"):
				list.get_node(String(other)).get_node("Role").show()
		
		# voting
		elif key == "decision":
			var decision = Network.get_own_data("decision")
			var is_voting = decision["type"] == "vote"
			for other in Network.get_ids():
				var select = list.get_node(String(other)).get_node("Select")
				select.visible = is_voting and Life.is_alive()
				if not is_voting:
					select.disconnect("toggled", self, "_on_toggle_select")
					select.pressed = false
					select.connect("toggled", self, "_on_toggle_select", [other])
				
				# disable decision 
				var selectable = Life.is_alive(other) and  decision["action"] != "finished" 
				select.disabled = not selectable
	
func _on_toggle_select(pressed, id):
	var list = $ScrollContainer/List
	var decision = Network.get_own_data("decision")
	if decision["type"] == "vote":
		var is_exclusive = "exclusive" in decision["params"]
		
		# only one selected at a time
		if is_exclusive:
			for other in Network.get_ids():
				var node = list.get_node(String(other)).get_node("Select")
				node.disconnect("toggled", self, "_on_toggle_select")
				node.pressed = pressed if id == other else false
				node.connect("toggled", self, "_on_toggle_select", [other])
		
		# multiple selected possible
		else:
			var node = list.get_node(String(id)).get_node("Select")
			node.disconnect("toggled", self, "_on_toggle_select")
			node.pressed = pressed
			node.connect("toggled", self, "_on_toggle_select", [id])
	
		# Update answer
		var answer = decision["answer"].duplicate(true)
		for id in Network.get_ids():
			var select = list.get_node(String(id)).get_node("Select")
			if select.pressed:
				if not id in answer: 
					answer.append(id)
			else:
				if id in answer:
					answer.erase(id)
		decision["answer"] = answer
		Network.set_own_data("decision", decision)
