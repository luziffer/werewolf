extends Node


# Role definitions 
const Definitions = {
	"Villager" : {
		"tex" : preload("res://Roles/images/villager.png"),
		"faction" : "Village",
		"default" : {}
	},
	
	"Werewolf" : {
		"tex" : preload("res://Roles/images/wulf.png"),
		"faction" : "Werewolves",
		"default" : {}
	},
	
	"Seer" : {
		"tex" : preload("res://Roles/images/seer.png"),
		"faction" : "Village",
		"default" : {"observee" : -1}
	},
	
	"Witch" : {
		"tex" : preload("res://Roles/images/witch.png"),
		"faction" : "Village",
		"default" : {
			"heal"   : 1,
			"poison" : 1,
			"rescuee" : -1,
			"victim"  : -1,
		}
	},
	
	"Amor" : {
		"tex" : preload("res://Roles/images/amor.png"),
		"faction" : "Village",
		"default" : {
			"lover1" : -1,
			"lover2" : -1,
		}
	},
}

func distribute_roles():
	if not get_tree().is_network_server(): return
	
	var villagers = Network.get_ids()
	var assignments = {}
	
	# Sample warewolves
	var max_warewolves = int( len(villagers)/ 3 )
	var n_warewolves = rand_range(1,max_warewolves+1)
	for _i in range(n_warewolves):
		var n = rand_range(0,len(villagers))
		var id = villagers[n]
		villagers.erase(id)
		assignments[id] = "Werewolf" 
	
	# Sample witches
	if len(villagers) > 0:
		var max_witches = int( len(villagers) / 5 )
		var n_witches = rand_range(1,max_witches+1)
		for _i in range(n_witches):
			var n = rand_range(0,len(villagers))
			var id = villagers[n]
			villagers.erase(id)
			assignments[id] = "Witch" 
	
	# Sample seers
	if len(villagers) > 0:
		var max_seers = int( len(villagers) / 5 )
		var n_seers = rand_range(1,max_seers+1)
		for _i in range(n_seers):
			var n = rand_range(0,len(villagers))
			var id = villagers[n]
			villagers.erase(id)
			assignments[id] = "Seer" 
	
	# Sample amor
	if len(villagers) > 0:
		var n = rand_range(0,len(villagers))
		var id = villagers[n]
		villagers.erase(id)
		assignments[id] = "Amor" 
	
	# Assign data
	for id in villagers:
		assignments[id] = "Villager"
	for id in assignments:
		var data = Network._data[id].duplicate()
		data["killed"] = -1
		data["memory"] = [id]
		
		var role = assignments[id]
		data["role"] = role
		data["factions"] = [ Roles.Definitions[role]["faction"] ]
		data["role_state"] = Roles.Definitions[role]["default"].duplicate(true)		
		
		if role == "Werewolf":
			var wolves = []
			for i in assignments:
				if assignments[i] == "Werewolf":
					wolves.append(i)
			data["memory"] = wolves
		
		elif role == "Witch":
			data["role_state"]["heal"] = randi() % 3
			data["role_state"]["poison"] = randi() % 3
		
		for attr in data:
			Network.set_data(id, attr, data[attr])

