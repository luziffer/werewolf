extends Node

signal dream_changed()

var _dream_timer : Timer
var _dream_text : String  = ""
var _dream_texts = [""]

func _ready():

	# Load dreams
	var f = File.new()
	f.open("res://Roles/dreams.txt", File.READ)
	var text = f.get_as_text()
	f.close()
	_dream_texts = []
	for msg in text.split("\n", false):
		while "  " in msg:
			msg = msg.replace("  ", " ")
		msg = msg.replace("\\n", "\n")
		if msg != "" and msg != " ":
			_dream_texts.append(msg)
	
	_dream_timer = Timer.new()
	add_child(_dream_timer)
	_dream_timer.connect("timeout", self, "_refresh_dream")
	Network.connect("handshake_complete", self, "_refresh_dream")
	_dream_timer.start(0)

func _refresh_dream():
	_dream_text = _dream_texts[rand_range(0, len(_dream_texts))]
	
	var culprit = "someone"
	var alive = Life.get_alive()
	if len(alive) > 0:
			
		# less than every second dream reliable 
		var reliable = randf() < .45
		
		# sample dream 
		var id = -1
		var wolves = Life.get_alive_with({"role" : "Werewolf"})
		if reliable and len(wolves) > 0:
			id = wolves[rand_range(0, len(wolves))]
		else:
			id = alive[rand_range(0, len(alive))]
		
		if id != get_tree().get_network_unique_id():
			culprit = Network.get_data(id,"name")
	_dream_text = _dream_text.replace("XXX", culprit)
	
	emit_signal("dream_changed")
	var wait_time = randf() * 10 + 5
	_dream_timer.start(wait_time)

func get_dream() -> String:
	return _dream_text
