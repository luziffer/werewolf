extends VBoxContainer

onready var Progress = $Choice/CenterContainer/ProgressBar

func _ready():
	Network.connect("data_changed", self, "_update_data")
	 

func _update_data(id, key):
	if key == "decision": 
		var alive = Life.get_alive()
		var decided = Decisions.have_finished(alive, true)
		
		Progress.min_value = 0
		Progress.max_value = len(alive)
		Progress.value = len(decided)
		Progress.get_node("Label").text = "%s / %s voted" % [len(decided), len(alive)]


func _on_vote():
	var decision = Network.get_own_data("decision")
	decision["action"] = "finished"
	Network.set_own_data("decision", decision)
