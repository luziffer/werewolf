extends VBoxContainer

func _ready():
	Dreams.connect("dream_changed", self, "_update_dream")
	_update_dream()

func _update_dream():
	$ScrollContainer/Dream.text = Dreams.get_dream()
