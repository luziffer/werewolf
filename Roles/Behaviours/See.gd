extends VBoxContainer


func _on_see():
	var decision = Network.get_own_data("decision")
	decision["action"] = "finished"
	Network.set_own_data("decision", decision)
