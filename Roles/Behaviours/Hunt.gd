extends VBoxContainer

func _on_hunt():
	$AnimationPlayer.stop(true)
	var decision = Network.get_own_data("decision")
	decision["action"] = "finished"
	Network.set_own_data("decision", decision)

