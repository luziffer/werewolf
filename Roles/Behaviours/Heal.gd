extends VBoxContainer

onready var Message = $ScrollContainer/Message
var _msg = ""

func _ready():
	_msg = Message.text
	Network.connect("data_changed", self, "_update_data")

func  _update_data(id, key):
	if id == 1 and key == "game_state":
		var victim = Network.get_data(1, "game_state")["victim"]
		victim = Network.get_data(victim, "name")
		Message.text = _msg.replace("VICTIM", victim)

func _on_heal():
	var decision = Network.get_own_data("decision")
	var victim = Network.get_data(1, "game_state")["victim"]
	decision["answer"] = victim
	decision["action"] = "finished"
	Network.set_own_data("decision",decision)


func _on_ignore():
	var decision = Network.get_own_data("decision")
	decision["answer"] = -2
	decision["action"] = "finished"
	Network.set_own_data("decision",decision)
