extends Panel

func hide_all():
	for c in get_children():
		c.hide()
	

func _ready():
	hide_all()
	Network.connect("data_changed", self, "_update")

func _update(id, key):
	
	if not Life.is_alive():
		hide_all()
		get_node("Dead").show()
		return
	
	var game_state = Network.get_data(1, "game_state")
	if id == 1 and key == "game_state" and not game_state["is_night"]:
			hide_all()
			get_node("Day").show()
	
	elif game_state["is_night"]:
		
		var role = Network.get_own_data("role")
		var role_state = Network.get_own_data("role_state")
		var sleep = true
		
		
		if role == "Werewolf":
			if game_state["victim"] == -1:
				sleep = false
				hide_all()
				$Hunt.show()
				var anim = $Hunt.get_node("AnimationPlayer")
				
				if not anim.is_playing():
					anim.stop()
				anim.play("Hunt")
		
		elif role == "Seer":
			if role_state["observee"] == -1:
				sleep = false
				hide_all()
				$See.show()
		
		elif role == "Witch":
			if role_state["rescuee"] == -1:
				if game_state["victim"] != -1:
					sleep = false
					hide_all()
					$Heal.show()
			elif role_state["victim"] == -1:
				sleep = false
				hide_all()
				$Poison.show()
		
		if sleep: 
			hide_all()
			$Sleep.show()
	

