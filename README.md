# Werewolf

![Icon](icon.png)

This is a free version of [Werewolf](https://en.wikipedia.org/wiki/Mafia_%28party_game%29).



## How to use

It is highly recommended to have a *video chat* or *voice call* conference going allowing you to talk to other players while playing.

If unsure how to build or which operating system to use just run the HTML5 version.
You can run it either on [itch.io](https://itch.io) or on your local machine with a test server.
To run it locally execute the following commands.

```
cd bin/Werewolf
python -m http.server
```

This starts a the test server at

**`http://0.0.0.0:8000/`**

You can simply type this address into your browser address bar to open the game.

## About

Why yet another Werewolf, aka. Mafia, aka. etc. game? Commonly the other game versions lack some features that are really important to keep the game interesting.

What makes this version so good are the following features

 - dead players can keep talking to others players
 - it is possible for characers to "peek" at night


## Debugging

Run multiple instances adjusted to screen size.(Only supported on Linux so far.)


```
├── bin
│   ├── run_multiple.py
│   └── Werewolf.x86_64
├── project.godot
└── ...
```

The `run_multiple.py` is an exectutable python script that runs binary in multiple subprocesses.

To use it simply navigate into binary folder. Type the commands marked by `$>` into your terminal.

```
$> cd bin
```

Specify the number of binary instances that should be run
```
$> ./run_multiple.py [-h] [--close] [--build] [-n NUMBER = 2] 

```
There is an option to closes all existing windows and one to create or rebuild the binary.

**Caveat!** All game output will be written to a single console! 


