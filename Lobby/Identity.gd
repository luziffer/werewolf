extends Node

var names = []
func _ready():
	_load_names()
	randomize()

# Read example name from text file
func _load_names():
	var f = File.new()
	f.open("res://Lobby/names.txt", File.READ)
	var text = f.get_as_text()
	f.close()
	for name in text.split("\n"):
		while "  " in name:
			name = name.replace("  ", " ")
		if name != "" and name != "  " and name[0] != "#":
			names.append(name)

# Modify example names
func sample_name() -> String:
	var sampled_name = names[rand_range(0, len(names))]
	
	# shuffle
	if randf() < .99:
		var words = sampled_name.split(" ") 
		var indices = []
		for i in range(len(words)):
			indices.append(i)
		indices.shuffle()
		sampled_name = ""
		for i in indices:
			sampled_name += words[i] + " "
		sampled_name[-1] = ""
	
	# mofify spaces
	if randf() < .2: 
		sampled_name = sampled_name.replace(" ", "_")
	elif randf() < .2: 
		sampled_name = sampled_name.replace(" ", "  ")
	elif randf() < .2: 
		sampled_name = sampled_name.replace(" ", "-")
	elif randf() < .2:
		sampled_name = sampled_name.replace(" ", "")
	elif randf() < .2:
		sampled_name = sampled_name.replace(" ", "..")
	
	# modify case
	if randf() < .2: 
		sampled_name = sampled_name.to_lower()
		if randf() < .1:
			for j in range(len(sampled_name)):
				var c = sampled_name[j] 
				sampled_name[j] = c.to_upper()
	elif randf() < .1:
		sampled_name = sampled_name.to_upper()
	
	# add spelling mistakes
	for letter in ["a", "e", "i", "o", "t", "n", "s"]:
		if randf() < .01:
			var r = ""
			for i in range(rand_range(1,3)):
				r += letter  
			sampled_name.replace(letter, r)
	if randf() < .05:
		sampled_name.replace("e", "ee")
	if randf() < .05:
		sampled_name.replace("i", "ii")
	if randf() < .05:
		sampled_name.replace("t", "tt")
	
	return sampled_name

# Random portrait  
# (will be shared via network, so don't use texture directy, ie. image file stream texture)
func sample_portrait() -> String: #-> Texture:
	var min_portrait = 0
	var max_portrait = 18
	var tex = "res://Lobby/portraits/portrait_%d.png" % rand_range(min_portrait,max_portrait+1)
	#tex = load(tex)
	return tex
