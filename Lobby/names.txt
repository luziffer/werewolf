somebody
nobody in particular
the one who shall not be named
What a name
Uuh okay
Away From Keyboard
Sally selling sea shells

# real names
Dimitry Davidoff
Mary Shelley
Bram Stoker
Stephenie Meyer
Joanne Rowling

# suspicious names
The Werecat
Wolfgang Wolf
Wolf Wolfington
Adalwolf Wolfbert
Beast of Gévaudan
Full Moon Dancer

# confusing names
Trust me, I'm the seer
Sleepy Villager
I am not Amor
Which Witch
100% no Werewolf
Not a wolf

# silly noble titles
Earl of Sandwich
Baron Hieronymus of Muenchhausen
Barroness of Mayonaise

# troll
Trust me, Bro
Viagrashop24.com
Earn 100.000$ in 10 Days with Simple Trick!
Copyright Enforcer 2000

# stolen names
Yabberwocky
Harry Potter
Hermione Granger
George Weasley
Fred Weasley
Sirius Black
Rubeus Hagrid
Luna Lovegood
Draco Malfoy
Minerva McGonagall
Quirinus Quirrell
Dolores Umbridge
Tom Marvolo Riddle
Neville Longbottom
Severus Snape
Argus Filch
Fenrir Greyback
Jacob Black
Bella Swan
Jasper Whitlock
Emmett Dale McCarty
Mary Alice Brandon
Esme Platt
Stregone Benefico
Edward Anthony Masen
Riley Biers
Victor Frankenstein
Frank N. Furter
Vlad Dracula De Ville
Dr. Acula
Lazor Wolf
