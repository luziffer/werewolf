extends VBoxContainer

onready var Template = $PlayerTemplate 

func _ready():
	Network.connect("handshake_complete", self, "_players_changed")


func _players_changed():
	var arrived = Network._data.keys()
	var present = []
	for c in get_children():
		if c != Template:
			present.append( int(c.name) )
	
	for id in arrived:
		if not id in present:
			var node = Template.duplicate(true)
			node.get_node("Name").text = Network._data[id]["name"]
			var tex =  Network._data[id]["portrait"]
			tex = load(tex)
			node.get_node("Portrait").texture = tex
			node.show()
			node.name = String(id)
			add_child(node)
	for id in present:
		if not id in arrived:
			var node = get_node(String(id))
			node.queue_free()
