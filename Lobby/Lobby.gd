extends Control

onready var Welcome = $Welcome
onready var Connection = $Connection
onready var Lounge = $Lounge

func _ready():
	get_tree().get_root().set_process(false)
	randomize()
	
	# show welcome screen 
	Welcome.show()
	Lounge.hide()
	Connection.hide()
	
	Lounge.get_node("Layout/Players/ScrollContainer/List/PlayerTemplate").hide()
	
	# random identity name
	var name = Identity.sample_name()
	Connection.get_node("Layout/Input/Name").text = name

func _on_Exit_pressed():
	get_tree().quit(0)

func _on_Cancel_pressed():
	Network.restart()
	Welcome.show()
	Connection.hide()
	Lounge.hide()

# Connection

func _on_Host_pressed():
	_choose_connection(true)
func _on_Join_pressed():
	_choose_connection(false)
	
func _choose_connection(is_host : bool):
	Connection.show()
	Welcome.hide()
	Lounge.hide()
	
	# always use host IP address 
	var i = Connection.get_node("Layout/Input")
	if is_host:
		i.get_node("Label_Address").hide()
		i.get_node("Address").hide()
		Lounge.get_node("Layout/Responses/Start").show()
	else:
		i.get_node("Label_Address").show()
		i.get_node("Address").show()
		Lounge.get_node("Layout/Responses/Start").hide()
	
	var label = "Host" if is_host else "Join"
	Connection.get_node("Layout/Responses/Proceed").text = label
	if is_host:
		 label = "Host a new game.\nPlease share your IP address."
	else:
		label = "Join an existing game.\nPlease ask host for their IP address."
	Connection.get_node("Layout/Message").text = label

func _on_Proceed_pressed():
	var i = Connection.get_node("Layout/Input")
	var is_host = not i.get_node("Address").visible
	var name = i.get_node("Name").text
	var port =  int( i.get_node("Port").text )
	
	if is_host:
		print("Hosting '", name, "' on ", port)
		Network.connect_host(port, name)
	else:
		var address = i.get_node("Address").text 
		print("Joining '", name, "' on ", port, " at ", address)
		Network.connect_client(address, port, name)
	
	Welcome.hide()
	Connection.hide()
	Lounge.show()


func _on_StartGame_pressed():
	Network.start_game()



#export(Texture) var _host_texture 
#export(Texture) var _client_texture 
#
#export(NodePath) var InputHost
#export(NodePath) var InputJoin
#
#func _ready():
#	get_tree().get_root().set_process(false)
#	randomize()
#	$Lobby/ScrollContainer/Players/PlayerTemplate.hide()
#	$Lobby.hide()
#	$Join.hide()
#	$Host.hide()
#	$Title.show()
#
#func _on_Exit_pressed():
#	get_tree().quit(0)
#
#func _on_Host_pressed():
#	$Title.hide()
#	$Host.show()
#
#	var input = get_node(InputHost)
#	input.get_node("Name").text = Identity.sample_random_name()
#
#func _on_Join_pressed():
#	$Title.hide()
#	$Join.show()
#	var input = get_node(InputJoin)
#	input.get_node("Name").text = Identity.sample_random_name()
#
#func _on_Back_pressed():
#	$Join.hide()
#	$Host.hide()
#	$Title.show()
#
#func _on_Join_chosen():
#	$Join.hide()
#	$Lobby.show()
#	$Lobby/Container/Start.hide()
#
#	var input = get_node(InputJoin)
#	var name = input.get_node("Name").text
#	var port = int( input.get_node("Port").text )
#	var address = input.get_node("Address").text
#
#	Network.connect("handshake_complete", self, "_update")
#	Network.connect_client(address, port, name)
#
#func _on_Host_chosen():
#	$Host.hide()
#	$Lobby.show()
#	$Lobby/Container/Start.show()
#
#	var input = get_node(InputHost)
#	var name = input.get_node("Name").text
#	var port = int( input.get_node("Port").text )
#
#	Network.connect("handshake_complete", self, "_update")
#	Network.connect_host(port, name)
#	yield(get_tree(), "connected_to_server")
#
#func _update():
#	var players = Network.players.keys()
#	var children = $Lobby/ScrollContainer/Players.get_children()
#
#	for c in children:
#		if c.name != "PlayerTemplate":
#			var id = int(c.name)
#			if not id in players:  
#				c.queue_free()
#	for player in players:
#		if not $Lobby/ScrollContainer/Players.has_node(String(player)):
#			_create_node_for_player(player)
#
#
#func _create_node_for_player(id):
#	var template = $Lobby/ScrollContainer/Players/PlayerTemplate
#	var player = $Lobby/ScrollContainer/Players/PlayerTemplate.duplicate()
#	player.name = String(id)
#
#	var texture = null
#	if id == 1:
#		texture = _host_texture
#	else:
#		texture = _client_texture
#	player.get_node("Container/Icon").texture = texture
#	player.get_node("Container/Icon/PlayerID").text = String(id)
#	texture = load(Network.players[id]["portrait"])
#	player.get_node("Container/Portrait").texture = texture 
#	player.get_node("Container/Name").text = Network.players[id]["name"]
#	player.show()
#	$Lobby/ScrollContainer/Players.add_child(player)
#
#
#func _on_Start():
#	Network.start_game()
#
#func _on_Quit_pressed():
#	Network.restart()


