extends Node

# Shared network data
var _data = {}
var _own_data = null

const _default_data = {
	"name" : "abc",
	"portrait" : "res://Lobby/portraits/portrait_0.png",
	
	"role" : "Villager",
	"role_state" : {},
	"factions" : [],
	
	"killed" : -1,
	"memory" : [],
	
	"decision" : {
		"answer" : null,
		"type" : "", 
		"action" : "",
		"params" : {}
	}
}

# Game state is stored by server (ie. player with network id = 1)
const _default_gamestate = {
	"round"    : 0,
	"is_night" : true,
	"victim"   : -1
}


# Data management

signal data_changed(id, key)

func get_ids():
	return _data.keys()

func get_own_data(key):
	return get_data(get_tree().get_network_unique_id(), key)
func set_own_data(key, value):
	set_data(get_tree().get_network_unique_id(), key, value)

func get_data(id, key):
	return _data[id][key]
	
func set_data(id, key, value):
	rpc("_set_data", id, key, value)
sync func _set_data(id, key, value):
	if typeof(value) in [TYPE_DICTIONARY, TYPE_ARRAY]:
		value = value.duplicate(true)
	_data[id][key] = value 
	emit_signal("data_changed", id, key)



# Connection 
signal handshake_complete
const MAX_PLAYERS = 24

func restart():
	print("Restarting game.")
	if get_tree().network_peer != null:
		get_tree().network_peer.close_connection()
		get_tree().network_peer = null
	_data = {}; _own_data = null
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://Lobby/Lobby.tscn")


# Initialize host
func connect_host(port, name):
	print("Connecting host")
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(port, 20)
	get_tree().network_peer = peer
	get_tree().refuse_new_network_connections = false
# warning-ignore:return_value_discarded
	get_tree().connect("network_peer_disconnected", self, "_leave_player")
# warning-ignore:return_value_discarded
	get_tree().connect("network_peer_connected", self, "_join_player")
	_own_data = _default_data.duplicate(true)
	_own_data["name"] = name
	_own_data["portrait"] = Identity.sample_portrait()
	_own_data["game_state"] = _default_gamestate.duplicate(true)
	_data[1] = _own_data
	emit_signal("handshake_complete")

# warning-ignore:unused_argument
func _leave_player(id):
	rpc("_remove_player", id)
	
sync func _remove_player(id):
	_data.erase(id)
	emit_signal("handshake_complete")

func _join_player(id):
	rpc("_add_player", id)

sync func _add_player(id):
	#_data[id] = _default_data.duplicate(true)
	emit_signal("handshake_complete")

func connect_client(address, port, name):
	_own_data = _default_data.duplicate(true)
	_own_data["name"] = name
	_own_data["portrait"] = Identity.sample_portrait()
	var peer = NetworkedMultiplayerENet.new()
	get_tree().connect("connected_to_server", self, "_connection_established")
	get_tree().connect("connection_failed", self, "_connection_unsuccessfull")
	get_tree().connect("server_disconnected", self, "_connection_torn")
	peer.create_client(address, port)
	get_tree().network_peer = peer
	get_tree().refuse_new_network_connections = false
	
func _connection_established():
	var id = get_tree().get_network_unique_id()
	_data[id] = _own_data.duplicate(true)
	rpc("_share_data", id, _own_data)

remote func  _share_data(id, data):
	if get_tree().is_network_server():
		for peer_id in _data:
			rpc_id(peer_id, "_share_data", id, data)
			rpc_id(id, "_share_data", peer_id, _data[peer_id])
	_data[id] = data
	emit_signal("handshake_complete")

func _connection_unsuccessfull():
	print("Unfortunately the connection could not be established...")
# warning-ignore:return_value_discarded
	get_tree().reload_current_scene()

func _connection_torn():
	print("Server disconnected!")
	var msg = AcceptDialog.new()
	msg.dialog_text = "Server disconnected."
	msg.get_ok().connect("pressed", self, "_restart")
	get_tree().get_current_scene().add_child(msg)
	msg.popup_centered()
	yield(msg, "confirmed")
	restart()

func start_game():
	Roles.distribute_roles()
	rpc("_start_game")

sync func _start_game():
	get_tree().refuse_new_network_connections = true
	get_tree().disconnect("network_peer_disconnected", self, "_leave_player")
	get_tree().disconnect("network_peer_connected", self, "_join_player")
	get_tree().change_scene("res://Rounds/Rounds.tscn")

func end_game():
	rpc("_end_game")

sync func _end_game():
	get_tree().change_scene("res://Award/Scoreboard.tscn")


