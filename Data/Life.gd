extends Node


var _dream_text : String  = ""
func dream() -> String:
	return _dream_text

# Connect decease to signal
signal died(id)
func _update_data(id, key):
	if key == "killed":
		if not is_alive(id):
			emit_signal("died", id)

#Check dead or alive?
func is_alive(id=null):
	if id == null: id = get_tree().get_network_unique_id()
	return Network.get_data(id, "killed") == -1


# Iterator routines
func get_alive() -> Array:
	var alive = []
	for id in Network.get_ids():
		if is_alive(id):
			alive.append(id)
	return alive

func get_alive_factions() -> Array:
	var factions = []
	var alive = get_alive()
	for id in alive:
		for faction in Network.get_data(id, "factions"):
			if not faction in factions:
				factions.append(faction)
	return factions

func get_alive_with(attributes := {}) -> Array:
	var player_ids = []
	var alive = get_alive()
	for id in alive:
		var valid = true
		for key in attributes:
			if Network.get_data(id,key) == attributes[key]:
				player_ids.append(id)
	return player_ids


# Dream updates
var _dream_timer : Timer
var _dream_texts = [""]
func _refresh_dream():
	_dream_text = _dream_texts[rand_range(0, len(_dream_texts))]
	var alive = get_alive()
	if len(alive) > 1: 
		
		if randf() < .75:
			var id = -1
			while id == -1:
				id = alive[rand_range(0, len(alive))]
				if id == get_tree().get_network_unique_id(): id = -1
			_dream_text = _dream_text.replace("XXX", Network.players[id]["name"])
		else:
			var wolves = get_alive_with({"role" : "Werewolf"})
			var id = wolves[rand_range(0, len(wolves))]
			_dream_text = _dream_text.replace("XXX", Network.players[id]["name"])
	
	var wait_time = randf() * 20 + 5
	_dream_timer.start(wait_time)

func _ready():
	Network.connect("data_changed", self, "_update_data")

	# Load dreams
	var f = File.new()
	f.open("res://Roles/dreams.txt", File.READ)
	var text = f.get_as_text()
	f.close()
	_dream_texts = []
	for msg in text.split("\n", false):
		while "  " in msg:
			msg = msg.replace("  ", " ")
		msg = msg.replace("\\n", "\n")
		if msg != "" and msg != " ":
			_dream_texts.append(msg)
	
	# Initialize dream timer
	_dream_timer = Timer.new()
	add_child(_dream_timer)
	_dream_timer.connect("timeout", self, "_refresh_dream")
	_dream_timer.start(0)
