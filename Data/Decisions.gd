extends Node


#signal decision_started(id)
#signal decision_finished(id)
#signal decision_cleared(id)

#func _ready():
#	Network.connect("data_changed", self, "_update_data")
	
#func _update_data(id, key):
#	if key == "decision":
#		var decision = Network.get_data(id, "decision")
#		if decision["action"] == "finished":
#			emit_signal("decision_finished", id)
#		elif decision["action"] == "start":
#			decision["action"] = "progress"
#			Network.set_data(id, "decision", decision)
#			emit_signal("decision_started", id)
#
func clear_decision(id):
	var data = Network._default_data["decision"].duplicate()
	Network.set_data(id, "decision", data)
#	rpc("_clear_decision", id)
#
#sync func _clear_decision(id):
#	emit_signal("decision_cleared", id)

func have_finished(ids, return_ids : bool = false) :
	if not return_ids:
		for id in ids:
			if Network.get_data(id, "decision")["action"] != "finished":
				return false
		return true
	else:
		var finished = []
		for id in ids:
			if Network.get_data(id, "decision")["action"] == "finished":
				finished.append(id)
		return finished

func get_decisions(ids) -> Dictionary:
	var responses = {}
	for id in ids:
		var decision = Network.get_data(id, "decision")
		var answer = decision["answer"]
		if typeof(answer) != TYPE_ARRAY:
			answer = [answer]
		for a in answer:
			if not a in responses:
				responses[a] = []
			responses[a].append(id)
	for id in ids:
		clear_decision(id)
	return responses

func count_responses(responses : Dictionary, relative = false) -> Dictionary:
	var count = {}
	for id in responses:
		count[id] = len(responses[id])
	
	if relative:
		var total = 0.0
		for id in count:
			total += count[id]
		if total > 0:
			for id in count:
				count[id] = count[id]/total
	return count

func get_winner(responses : Dictionary, draw : bool = false):
	var candidates = responses.keys()
	if len(candidates) == 0: 
		return [] if draw else -2
	 
	var count = count_responses(responses)
	var winners = []
	var c = count[candidates[0]]
	for id in candidates:
		if count[id] == c:
			winners.append(id)
		elif count[id] > c:
			winners = [id]
			c = count[id]
	if not draw:
		winners.shuffle()
		winners = winners[0] 
	return winners
